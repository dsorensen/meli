package com.ar.pages;

import net.bytebuddy.asm.Advice;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.internal.WebElementToJsonConverter;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class HomePage {
    private WebDriver driver;

    //Page URL
    private static String PAGE_URL="https://www.mercadolibre.com.ar";

    //Locators

    //Apply as Developer Button
    @FindBy(how = How.LINK_TEXT, using = "Ver promociones bancarias")
    private WebElement promociones;

    //Constructor
    public HomePage(WebDriver driver){
        this.driver=driver;
        driver.get(PAGE_URL);
        //Initialise Elements
        PageFactory.initElements(driver, this);
    }


    @FindBy(className = "andes-modal-dialog__header-title")
    private WebElement heading;

    public void clickpromo(){

        promociones.click();

    }

    @FindBy(className = "nav-menu-categories-link")
    private WebElement categoria;
    public void clickcategoria()

    {
        categoria.click();

    }

    @FindBy(how = How.LINK_TEXT , using = "TVs")
    private WebElement tv;
    public void clicktv()

    {
        tv.click();

    }

    @FindBy(className ="ui-dropdown__link")
    private WebElement dropdown;

    @FindBy(how = How.XPATH , using = "//a[contains(text(),'Menor precio')]")
    private WebElement menorPrecio;

    public void clickOrdenarMenorPrecio()
    {
        dropdown.click();
        menorPrecio.click();
    }


    @FindBy(className = "price-tag-fraction")
    private WebElement price;
    @FindBy(className = "results-items")
    private List<WebElement> elements;

    public void obtainPriceLastElement()
    {
       // List<WebElement> el = driver.findElements(By.className("results-item"));
        String sprice = "";
        int sizeofResults = elements.size()-1;
       // elements.get(sizeofResults).findElement(By.className("main-title")).click();
      //  sprice = price.getText()
       System.out.println(sizeofResults);
    }

    public boolean isPageOpened(){
        return heading.getText().toString().contains("Promociones bancarias");
    }


    @FindBy(name = "as_word")
    private WebElement tipeo;
    public void typeSomething(String something)
    {
        tipeo.sendKeys(something);
        tipeo.sendKeys(Keys.RETURN);






    }



}